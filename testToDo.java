package testclasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testToDO
{
	
	//Pre-conditions:
	
	WebDriver driver;
	
	@BeforeSuite
	public void setUp()
	{
		System.out.println("setup system property for chrome");
		String baseURL = "http://www.google.com";
		System.setProperty("webdriver.chrome.driver", "/Users/erickmena/Desktop/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		//driver.manage().window().fullscreen();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseURL);
	}
	
	@BeforeMethod
	public void tearUp()
	{
		System.out.println("Beggining Method");
		
	}
	
	//Test Case:
	
	@Test(priority=1)
	public void googleTitleTest()
	{
		System.out.println("Google Title Test");
		String title1 = driver.getTitle();
		String title1Test = "Google";
		Assert.assertEquals(title1, title1Test);
		
	}
	
	@Test(priority=2)
	public void ToDOTitleTest() throws AWTException
	{
		System.out.println("ToDO Title Test");
		driver.get("https://qa-test.avenuecode.com/user-stories");
		String title2 = driver.getTitle();
		String title2Test = "ToDO App";
		Assert.assertEquals(title2, title2Test);
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.ARROW_DOWN));

		
	}
	
	@Test(priority=3)
	public void myTasksLinkTest()
	{
		System.out.println("My Tasks Link");
		boolean linkTest1 = driver.findElement(By.linkText("My Tasks")).isDisplayed();
		Assert.assertEquals(true, linkTest1);
		if(linkTest1)
		{
			System.out.println("Get started link is displaed and working");
		}
	}
	
	
	//Post-conditions:
	
	@AfterTest
	public void deleteAllCookies()
	{
		System.out.println("Delete All Cookies");
		driver.manage().deleteAllCookies();
	}
	
	@AfterClass
	public void closeBrowser()
	{
		System.out.println("Close Browser");
		//driver.close();
	}
	
	@AfterMethod
	public void tearDown()
	{
		System.out.println("Ending Method");
		//driver.quit();
	}
	
	@AfterSuite
	public void generateTestReport() throws AWTException
	{
		System.out.println("Generated Test Report");
		driver.get("file:///Users/erickmena/eclipse-workspace/WebSiteTesting/test-output/index.html");
		driver.findElement(By.cssSelector(""));
	}
	
}